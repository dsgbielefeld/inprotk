package inpro.sphinx.frontend;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.io.IOUtils;

import com.google.protobuf.ByteString;

import edu.cmu.sphinx.frontend.BaseDataProcessor;
import edu.cmu.sphinx.frontend.Data;
import edu.cmu.sphinx.frontend.DataEndSignal;
import edu.cmu.sphinx.frontend.DataProcessingException;
import edu.cmu.sphinx.frontend.DataStartSignal;
import edu.cmu.sphinx.frontend.DoubleData;
import edu.cmu.sphinx.frontend.util.DataUtil;
import edu.cmu.sphinx.frontend.util.Utterance;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import inpro.sphinx.frontend.RsbStreamInputSource.RecordingThread;
import rsb.AbstractEventHandler;
import rsb.Event;
import rsb.Factory;
import rsb.Listener;
import rst.audition.SoundChunkType.SoundChunk;

public class AudioStreamQueue extends BaseDataProcessor {
	
	
	private int msecPerRead = 10; //default value
	private long totalSamplesRead = 0; //start value
	private boolean signed = true;
	private int frameSizeInBytes;
	private boolean bigEndian = false;
	
	
	private BlockingQueue<SoundChunk> soundChunks;
	private BlockingQueue<Data> audioList;
	private AudioInputStream audioInputStream = null;
	private AudioFormat audioFormat;
	private Utterance currentUtterance;
	private volatile boolean recording;
	private volatile boolean utteranceEndReached = true;
	private RecordingThread recorder;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		
	}
	
	public AudioStreamQueue() {
		soundChunks = new LinkedBlockingQueue<SoundChunk>();
		audioList = new LinkedBlockingQueue<Data>();
		this.audioFormat = getConfAudioFormat();
		float sec = msecPerRead / 1000.f;
		frameSizeInBytes = (audioFormat.getSampleSizeInBits() / 8)
				* (int) (sec * audioFormat.getSampleRate())
				* audioFormat.getChannels();
	}
	
	public void initialize() {
		super.initialize();
	}
	
	/**
	 * Returns the format of the audio recorded by the RSB SoundChunks
	 * 
	 * @return the current AudioFormat
	 */
	public AudioFormat getAudioFormat() {
		return audioFormat;
	}

	/**
	 * Returns the current Utterance.
	 * 
	 * @return the current Utterance
	 */
	public Utterance getUtterance() {
		return currentUtterance;
	}

	/**
	 * Returns true if this StreamInput is recording.
	 * 
	 * @return true if this StreamInput is recording, false otherwise
	 */
	public boolean isRecording() {
		return recording;
	}

	/**
	 * Starts recording audio.
	 * 
	 * @return true if the recording started successfully; false otherwise
	 */
	public synchronized boolean startRecording() {
		if (recording) {
			return false;
		}

		utteranceEndReached = false;

		assert (recorder == null);
		recorder = new RecordingThread("Microphone");
		recorder.start();
		recording = true;
		return true;
	}

	/**
	 * Stops recording audio. This method does not return until recording has
	 * been stopped and all data has been read from the RSB listener.
	 */
	public synchronized void stopRecording() {

		if (recorder != null) {
			recorder.stopRecording();
			recorder = null;
		}
		recording = false;
	}
	
	
//	This should be called externally
	public void addToQueue(byte[] data, int offset, int length) {
		update(data, offset, length);
//		SoundChunk.Builder sc = SoundChunk.newBuilder();
//		// set audio data
//		ByteString bs = ByteString.copyFrom(data);
//		sc.setData(bs);
//		sc.setChannels(audioFormat.getChannels());
//		sc.setRate((int) audioFormat.getSampleRate());
//		sc.setSampleCount(length);
//		
//		try {
//			if (soundChunks != null)
//				soundChunks.put(sc.build());
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
	}
	
//	public Data readData() throws DataProcessingException, IOException, InterruptedException, UnsupportedAudioFileException {
////		byte[] data = new byte[frameSizeInBytes];
//		if (audioInputStream == null || audioInputStream.available() <= 0) {
//			byte[] audioSource = queue.take().data;
//			InputStream byteArrayInputStream = new ByteArrayInputStream(
//					audioSource);
//			audioInputStream = new AudioInputStream(byteArrayInputStream,
//					audioFormat, audioSource.length);
//		}
//
//		audioInputStream = AudioSystem.getAudioInputStream(audioFormat, audioInputStream); //convert to the right format!
//		byte[] data= IOUtils.toByteArray(audioInputStream);
//		
//		int channels = audioFormat.getChannels();
//		long firstSampleNumber = totalSamplesRead / channels;
//		int sampleSizeInBytes = audioFormat.getSampleSizeInBits() / 8;
//		
//		int numBytesRead = audioInputStream.read(data, 0, data.length);
//		if (numBytesRead <= 0) {
//			return null;
//		}
//		
//		if (numBytesRead != frameSizeInBytes) {
//			if (numBytesRead % sampleSizeInBytes != 0) {
//				throw new Error("Incomplete sample read.");
//			}
//			data = Arrays.copyOf(data, numBytesRead);
//		}
//		
//		double[] samples = DataUtil.littleEndianBytesToValues(data, 0, data.length, sampleSizeInBytes, signed);
//		
//		return (new DoubleData(samples, (int) audioFormat.getSampleRate(),
//				firstSampleNumber));
//	}
//	
//private boolean ff = false;
//	public Data readData() throws DataProcessingException, IOException, InterruptedException, UnsupportedAudioFileException {
////		byte[] data = new byte[frameSizeInBytes];
//		if (ff) return null;
//		File f = new File("12345.wav");
//		
////		byte[] data = new byte[frameSizeInBytes];
////		if (audioInputStream == null || audioInputStream.available() <= 0) {
////			byte[] audioSource = queue.take().data;
////			InputStream byteArrayInputStream = new ByteArrayInputStream(
////					audioSource);
//			audioInputStream = AudioSystem.getAudioInputStream(f);
////		}
//
//		int channels = audioFormat.getChannels();
//		long firstSampleNumber = totalSamplesRead / channels;
//		
//		audioInputStream = AudioSystem.getAudioInputStream(audioFormat, audioInputStream); //convert to the right format!
////		int numBytesRead = audioInputStream.read(data, 0, data.length);
//		byte[] data= IOUtils.toByteArray(audioInputStream);
////		if (numBytesRead <= 0) {
////			return null;
////		}
//		// notify the waiters upon start
////		if (!started) {
////			synchronized (this) {
////				started = true;
////				notifyAll();
////			}
////		}
//
////		if (numBytesRead <= 0) {
////			return null;
////		}
//		int sampleSizeInBytes = audioFormat.getSampleSizeInBits() / 8;
////		totalSamplesRead += (numBytesRead / sampleSizeInBytes);
////
////		if (numBytesRead != frameSizeInBytes) {
////			if (numBytesRead % sampleSizeInBytes != 0) {
////				throw new Error("Incomplete sample read.");
////			}
////			data = Arrays.copyOf(data, numBytesRead);
////		}
//		double[] samples;
//
////		if (bigEndian) {
////			samples = DataUtil.bytesToValues(data, 0, data.length,
////					sampleSizeInBytes, signed);
////		} else {
//			samples = DataUtil.littleEndianBytesToValues(data, 0,
//					data.length, sampleSizeInBytes, signed);
////		}
//			ff = true;
//			
//		return (new DoubleData(samples, (int) audioFormat.getSampleRate(),
//				firstSampleNumber));
//	}
	
	
	private void update(byte[] data, int offset, int length) {
	 try {
			// The temporary file that contains our captured audio stream
			File f = new File("out.wav");

			// if the file already exists we append it.
			if (f.exists()) {

				// two clips are used to concat the data
				 AudioInputStream clip1 = AudioSystem.getAudioInputStream(f);
				 AudioInputStream clip2 = AudioSystem.getAudioInputStream(new ByteArrayInputStream(data));
				 
				 // use a sequenceinput to cat them together
				 AudioInputStream appendedFiles = 
                            new AudioInputStream(
                                new SequenceInputStream(clip1, clip2),     
                                clip1.getFormat(), 
                                clip1.getFrameLength() + clip2.getFrameLength());
				 
//				 appendedFiles = AudioSystem.getAudioInputStream(audioFormat, appendedFiles);

				 // write out the output to a temporary file
                    AudioSystem.write(appendedFiles, 
                            AudioFileFormat.Type.WAVE,
                            new File("out2.wav"));

                    // rename the files and delete the old one
                    File f1 = new File("out.wav");
                    File f2 = new File("out2.wav");
                    f1.delete();
                    f2.renameTo(new File("out.wav"));
			} else {
				FileOutputStream fOut = new FileOutputStream("out.wav",true);
				fOut.write(data);
				fOut.close();
			}			
		} catch (Exception e) {	
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * This Thread records audio, and caches them in an audio buffer.
	 */
	class RecordingThread extends Thread {

		private boolean done;
		private volatile boolean started;
		private long totalSamplesRead;
		private final Object lock = new Object();

		/**
		 * Creates the thread with the given name
		 * 
		 * @param name
		 *            the name of the thread
		 */
		public RecordingThread(String name) {
			super(name);
		}

		/**
		 * Starts the thread, and waits for recorder to be ready
		 */
		@Override
		public synchronized void start() {
			started = false;
			super.start();
		}

		/**
		 * Stops the thread. This method does not return until recording has
		 * actually stopped, and all the data has been read from the RSB
		 * listener.
		 */
		public void stopRecording() {
			try {
				synchronized (lock) {
					while (!done) {
						lock.wait();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Implements the run() method of the Thread class. Records audio, and
		 * cache them in the audio buffer.
		 */
		@Override
		public void run() {
			soundChunks.clear();
			totalSamplesRead = 0;
			logger.info("started recording");

			audioList.add(new DataStartSignal((int) audioFormat.getSampleRate()));
			logger.info("DataStartSignal added");
			try {
				while (!done) {
					Data data = readData();
					if (data == null) {
						done = true;
						break;
					}
					audioList.add(data);
				}

			} catch (IOException ioe) {
				logger.warning("IO Exception " + ioe.getMessage());
				ioe.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long duration = (long) (((double) totalSamplesRead / (double) audioFormat
					.getSampleRate()) * 1000.0);

			audioList.add(new DataEndSignal(duration));
			logger.info("DataEndSignal ended");
			logger.info("stopped recording");

			synchronized (lock) {
				lock.notify();
			}
		}

		/**
		 * Reads one frame of audio data, and adds it to the given Utterance.
		 * 
		 * @param utterance
		 * @return an Data object containing the audio data
		 * @throws java.io.IOException
		 * @throws InterruptedException
		 */
		private Data readData() throws IOException,
				InterruptedException {

			// Read the next chunk of data from the input stream.
			byte[] data = new byte[frameSizeInBytes];
			if (audioInputStream == null || audioInputStream.available() <= 0) {
				byte[] audioSource = soundChunks.take().getData().toByteArray();
				InputStream byteArrayInputStream = new ByteArrayInputStream(
						audioSource);
				audioInputStream = new AudioInputStream(byteArrayInputStream,
						audioFormat, audioSource.length);
			}

//			audioInputStream = AudioSystem.getAudioInputStream(audioFormat, audioInputStream); //convert to the right format!
			int channels = audioFormat.getChannels();
			long firstSampleNumber = totalSamplesRead / channels;

			int numBytesRead = audioInputStream.read(data, 0, data.length);
			if (numBytesRead <= 0) {
				return null;
			}
			// notify the waiters upon start
			if (!started) {
				synchronized (this) {
					started = true;
					notifyAll();
				}
			}

			if (logger.isLoggable(Level.FINE)) {
				logger.info("Read " + numBytesRead
						+ " bytes from audio stream.");
			}
			if (numBytesRead <= 0) {
				return null;
			}
			int sampleSizeInBytes = audioFormat.getSampleSizeInBits() / 8;
			totalSamplesRead += (numBytesRead / sampleSizeInBytes);

			if (numBytesRead != frameSizeInBytes) {
				if (numBytesRead % sampleSizeInBytes != 0) {
					throw new Error("Incomplete sample read.");
				}
				data = Arrays.copyOf(data, numBytesRead);
			}
//			if (numBytesRead > 320) numBytesRead = 320; // google ASR doesn't like it when there are more then 320
			double[] samples;

			if (bigEndian) {
				samples = DataUtil.bytesToValues(data, 0, numBytesRead,
						sampleSizeInBytes, signed);
			} else {
				samples = DataUtil.littleEndianBytesToValues(data, 0,
						data.length, sampleSizeInBytes, signed);
			}

			return (new DoubleData(samples, (int) audioFormat.getSampleRate(),
					firstSampleNumber));
		}
	}

	/**
	 * Clears all cached audio data.
	 */
	public void clear() {
		audioList.clear();
	}

	/**
	 * Reads and returns the next Data object from RSB listener, return null if
	 * there is no more audio data. All audio data captured in-between
	 * <code>startRecording()</code> and <code>stopRecording()</code> is cached
	 * in an Utterance object. Calling this method basically returns the next
	 * chunk of audio data cached in this Utterance.
	 * 
	 * @return the next Data or <code>null</code> if none is available
	 * @throws DataProcessingException
	 *             if there is a data processing error
	 */
	@Override
	public Data getData() throws DataProcessingException {
		Data output = null;

//		if (!utteranceEndReached) {
			try {
				output = audioList.take();
			} catch (InterruptedException ie) {
				throw new DataProcessingException(
						"cannot take Data from audioList", ie);
			}
			if (output instanceof DataEndSignal) {
//				System.out.println("UTTERANCE END!");
//				utteranceEndReached = true;
			}
//		}

		//getTimer().stop();
		return output;
	}

	/**
	 * Returns true if there is more data in the RSB listener. This happens
	 * either if the a DataEndSignal data was not taken from the buffer, or if
	 * the buffer is not yet empty.
	 * 
	 * @return true if there is more data
	 */
	public boolean hasMoreData() {
		return !(utteranceEndReached && audioList.isEmpty());
	}

	private static AudioFormat getConfAudioFormat() {
		float sampleRate = 16000.0F;
		// 8000,11025,16000,22050,44100
		int sampleSizeInBits = 16;
		// 8,16
		int channels = 1;
		// 1,2
		boolean signed = true;
		// true,false
		boolean bigEndian = false;
		// true,false
		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed,
				bigEndian);
	}
	

}
