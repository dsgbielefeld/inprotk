package inpro.incremental.unit;

import inpro.incremental.unit.IU;
import sium.nlu.stat.Distribution;

public class SlotIU extends IU {
	
	private Distribution<String> distribution;
	private String name;
	private boolean head;
	
	public SlotIU(String name, Distribution<String> dist) {
		setName(name);
		setDistribution(dist);
	}

	public SlotIU() {
	}

	@Override
	public String toPayLoad() {
//		if (getDistribution() == null) 
//		if (getDistribution().isEmpty()) 
			return String.format("%s", getName());
//		return String.format("%s", getDistribution());
	}
	
	public String toString() {
//		if (getDistribution() == null) return null;
//		if (getDistribution().isEmpty())  return String.format("%s", getName());
		return String.format("%s", getName());
	}

	public Distribution<String> getDistribution() {
		return distribution;
	}


	public void setDistribution(Distribution<String> distribution) {
		this.distribution = distribution;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setIsHead(boolean head) {
		this.head = head;
	}
	
	public boolean isHead() {
		return this.head;
	}

	public double getConfidence() {
		if (this.getDistribution().isEmpty()) return 0.0;
		return this.getDistribution().getArgMax().getProbability();
	}

}
