package inpro.incremental.source;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Test;

import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import inpro.apps.SimpleReco;
import inpro.apps.util.RecoCommandLineParser;

public class VoiceActivityDetectorTest {
	
	@Test(timeout=60000)
	public void testBuiltInSLM()  throws PropertyException, IOException, UnsupportedAudioFileException {
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() { 
		    	try {
		    		Files.deleteIfExists(new File("testir.inc_reco").toPath());
		    	} catch (IOException e) {
		    		e.printStackTrace();
			} }
		});
		
//		testConfiguration("-F", "file:res/DE_1234.wav"); // test with built-in SLM
		testConfiguration("-M"); 
		
//		Files.deleteIfExists(new File("testir.inc_reco").toPath());

	}
	
	
	private void testConfiguration(String... recoArgs) throws PropertyException, IOException, UnsupportedAudioFileException {
		RecoCommandLineParser clp = new RecoCommandLineParser(recoArgs);
		System.out.println(clp.getConfigURL());
		ConfigurationManager cm = new ConfigurationManager(clp.getConfigURL());
		SimpleReco simpleReco = new SimpleReco(cm, clp);
		VoiceActivityDetector vad = (VoiceActivityDetector) cm.lookup("vad");
		
		simpleReco.recognizeOnce();
		simpleReco.getRecognizer().deallocate();
	}

}
